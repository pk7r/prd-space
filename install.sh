#!/bin/bash

update_var() {
  local var="$1"
  local val="$2"
  local file="$3"
  sed s/"$var"/"$val"/g "$file"
}

# Function to update the TUNNEL_TOKEN variable in docker-compose.yml
update_tunnel_token() {
    local new_token="$1"
    update_var @TUNNEL_TOKEN "$new_token" ./docker-compose.yml
}

# Function to create variables for PUBLIC URLs
create_public_urls() {
    local domain="$1"
    PUBLIC_SPACE_URL="space.$domain"
    PUBLIC_PACKAGES_URL="packages.$domain"
    PUBLIC_GIT_URL="git.$domain"
}

# Function to update variables in config files
update_config_files() {
  update_var @PUBLIC_SPACE_URL "$PUBLIC_SPACE_URL" ./config/space.on-premises.conf
  update_var @PUBLIC_PACKAGES_URL "$PUBLIC_PACKAGES_URL" ./config/space.on-premises.conf
  update_var @PUBLIC_SPACE_URL "$PUBLIC_SPACE_URL" ./config/packages.on-premises.conf
  update_var @PUBLIC_PACKAGES_URL "$PUBLIC_PACKAGES_URL" ./config/packages.on-premises.conf
  update_var @PUBLIC_SPACE_URL "$PUBLIC_SPACE_URL" ./config/vcs.on-premises.properties
  update_var @PUBLIC_GIT_URL "$PUBLIC_GIT_URL" ./config/vcs.on-premises.properties
}

# Prompt the user for the new tunnel token
read -p "Enter the Cloudflare tunnel token: " new_tunnel_token

# Call the function to update the token
update_tunnel_token "$new_tunnel_token"
echo "TUNNEL_TOKEN updated in docker-compose.yml"

# Prompt the user for the domain
read -p "Enter the domain (e.g. example.com): " domain

# Call the function to create public URLs
create_public_urls "$domain"
echo "PUBLIC_SPACE_URL: $PUBLIC_SPACE_URL"
echo "PUBLIC_PACKAGES_URL: $PUBLIC_PACKAGES_URL"
echo "PUBLIC_GIT_URL: $PUBLIC_GIT_URL"

# Call the function to update variables in config files
update_config_files
echo "Config files updated."

echo "Initializing Compose file..."
docker-compose -p space up -d
echo "Production ready Space On-Premises setup executed."